[Migrated to Github](https://github.com/bogeymin/servicestarfish)

# Starfish

Or, Service Starfish is a simple service management tool based on the Wagtail CMS. It 
covers 5 aspects of managing services:

1. Classification (Process)
2. Menu (Process)
3. Product (Technology)
4. Assignment (People)
5. Review (Process)

To install:

	pip install https://bitbucket.org/bogeymin/servicestarfish/get/master.zip;

Or in your requirements file:

	-e git+https://bitbucket.org/bogeymin/servicestarfish/get/master.zip

Or in your ``setup.py`` file:

	install_requires=["servicestarfish"],
	dependency_links=[
		"https://bitbucket.org/bogeymin/servicestarfish/get/master.zip#egg=servicestarfish",
	]