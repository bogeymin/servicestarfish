# Imports

from django.utils.translation import ugettext_lazy as _
from wagtail.contrib.modeladmin.options import ModelAdminGroup, modeladmin_register
from servicestarfish.menus.wagtail_hooks import GroupingAdmin, RequestAdmin
from servicestarfish.services.wagtail_hooks import CategoryAdmin, LifecycleAdmin
from servicestarfish.technologies.wagtaiL_hooks import ProductAdmin, VendorAdmin

# Models


class StarfishAdminGroup(ModelAdminGroup):
    menu_label = _("Services")
    menu_icon = "folder-inverse"
    menu_order = 200
    items = [
        CategoryAdmin,
        GroupingAdmin,
        LifecycleAdmin,
        ProductAdmin,
        RequestAdmin,
        VendorAdmin,
    ]

modeladmin_register(StarfishAdminGroup)
