# Imports

from wagtail.contrib.modeladmin.options import ModelAdmin
from models import Category, Lifecycle

# Models


class CategoryAdmin(ModelAdmin):
    model = Category
    menu_icon = "folder-inverse"

    list_display = [
        "label",
        "description",
    ]


class LifecycleAdmin(ModelAdmin):
    model = Lifecycle
    menu_icon = "folder-inverse"

    list_display = [
        "label",
        "description",
        "sort_order",
    ]
