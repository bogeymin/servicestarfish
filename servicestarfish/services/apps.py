from __future__ import unicode_literals
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ServicesConfig(AppConfig):
    name = 'servicestarfish.services'
    label = "servicestarfish_services"
    verbose_name = _("Services")
