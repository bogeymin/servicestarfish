# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-15 01:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('servicestarfish_services', '0002_auto_20161015_0013'),
    ]

    operations = [
        migrations.CreateModel(
            name='SupportingService',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('link_external', models.URLField(blank=True, help_text='Enter the full URL if the service is not maintained in this tool.', max_length=1024, null=True, verbose_name='External Link')),
                ('link_page', models.ForeignKey(blank=True, help_text='Link to another service maintained in this tool.', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='dependent_services', to='servicestarfish_services.ServicePage', verbose_name='Link to Service')),
                ('page', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='supporting_services', to='servicestarfish_services.ServicePage')),
            ],
            options={
                'ordering': ['sort_order'],
                'verbose_name': 'Supporting Service',
                'verbose_name_plural': 'Supporting Services',
            },
        ),
    ]
