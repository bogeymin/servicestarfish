# Imports

from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.models import Group
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import models
from django.utils.translation import ugettext_lazy as _
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from taggit.models import TaggedItemBase
from wagtail.wagtailadmin.edit_handlers import InlinePanel, FieldPanel, MultiFieldPanel, PageChooserPanel
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailsearch import index
from wagtailmodelchooser.edit_handlers import register_chooser_for_model
from lemurlink.models import RelatedLinkModel
from taxonomytiger.models import StringLookupModel
from servicestarfish.technologies.models import Product, ProductChooserPanel
from servicestarfish.menus.models import Request, RequestChooserPanel

GroupChooserPanel = register_chooser_for_model(Group, title_field="name")

# Exports

__all__ = (
)

# Constants

AUTH_USER_MODEL = settings.AUTH_USER_MODEL

# Lookups


class Category(StringLookupModel):
    """A service category. Typically, a line of service."""

    class Meta:
        ordering = ["label"]
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

CategoryChooserPanel = register_chooser_for_model(Category)


class Lifecycle(StringLookupModel):
    """A lifecycle represents a phase or stage in which a service may be described."""

    sort_order = models.PositiveSmallIntegerField(
        _("sort oder"),
        help_text=_("Order in which the lifecycle should appear.")
    )

    panels = StringLookupModel.panels + [
        FieldPanel("sort_order"),
    ]

    class Meta:
        ordering = ["sort_order"]
        verbose_name = _("Lifecycle")
        verbose_name_plural = _("Lifecycles")

LifecycleChooserPanel = register_chooser_for_model(Lifecycle)

# Models


class ServiceIndexPage(Page):
    """An index page for services. One might treat this like a service catalog."""

    intro = RichTextField(
        _("intro"),
        blank=True,
        help_text=_("Brief introduction appearing before the package list."),
        null=True
    )

    is_catalog = models.NullBooleanField(
        _("catalog"),
        help_text=_("Indicates this page represents a service catalog.")
    )

    content_panels = Page.content_panels + [
        FieldPanel("intro", classname="full"),
        FieldPanel("is_catalog"),
    ]

    search_fields = Page.search_fields + [
        index.SearchField("intro"),
    ]

    subpage_types = [
        "ServiceIndexPage",
        "ServicePage",
    ]

    def get_subpages(self, criteria=None):
        """Get a list of packages."""
        qs = ServicePage.objects.live().descendant_of(self)

        if isinstance(criteria, dict):
            qs = qs.filter(**criteria)

        return qs

    def get_categories(self):
        a = list()

        qs = Category.objects.all()
        for category in qs:
            services = ServicePage.objects.live().descendant_of(self).filter(category=category)
            a.append((category, services))

        return a

    def get_context(self, request, *args, **kwargs):
        context = super(ServiceIndexPage, self).get_context(request, *args, **kwargs)

        if self.is_catalog:
            context['categories'] = None
            objects = ServiceIndexPage.objects.live().descendant_of(self)
        else:
            context['categories'] = self.get_categories()

            # Get the objects.
            objects = self.get_subpages()

        # Pagination
        page = request.GET.get('page')
        entries_per_page = 9
        paginator = Paginator(objects, entries_per_page)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)

        # Update template context
        context['active_page'] = "services"
        context['subpages'] = objects

        return context

    @property
    def subpages(self):
        return self.get_subpages()


class ServicePageAgreement(RelatedLinkModel, Orderable):
    """Associate an SLA or OLA with a service."""

    covers_customer = models.NullBooleanField(
        _("customer"),
        help_text=_("Indicates this agreement is with a specific customer.")
    )

    covers_organization = models.NullBooleanField(
        _("organization"),
        help_text=_("Indicates this agreement is with the whole organization.")
    )

    covers_service = models.NullBooleanField(
        _("service"),
        help_text=_("Indicates this agreement is specific to the selected service.")
    )

    is_multi_level = models.NullBooleanField(
        _("multi-level"),
        help_text=_("Indicates this is a multi-level agreement.")
    )

    page = ParentalKey("ServicePage", related_name="agreements")

    CONTRACT = "contract"
    OLA = "OLA"
    SLA = "SLA"
    TYPE_CHOICES = (
        (OLA, _("Operating Level Agreement")),
        (SLA, _("Service Level Agreement")),
        (CONTRACT, _("Underpinning Contract")),
    )
    type = models.CharField(
        _("type"),
        choices=TYPE_CHOICES,
        default=SLA,
        help_text=_("Select the type of agreement."),
        max_length=32
    )

    panels = RelatedLinkModel.panels + [
        FieldPanel("type"),
        MultiFieldPanel(
            [
                FieldPanel("covers_organization"),
                FieldPanel("covers_customer"),
                FieldPanel("covers_service"),
            ],
            heading=_("Scope")
        )
    ]

    class Meta:
        verbose_name = _("Service Agreement")
        verbose_name_plural = _("Service Agreements")

    def save(self, *args, **kwargs):
        a = list()
        if self.covers_customer:
            a.append(1)

        if self.covers_organization:
            a.append(1)

        if self.covers_service:
            a.append(1)

        if sum(a) >= 2:
            self.is_multi_level = True
        else:
            self.is_multi_level = False

        super(ServicePageAgreement, self).save(*args, **kwargs)


class ServicePageProduct(Orderable):
    """Connect products with services."""

    is_primary = models.NullBooleanField(
        _("primary"),
        help_text=_("Indicates this is the primary product for the service.")
    )

    # Products are connected to the business (or other) service from the ServicePage.
    page = ParentalKey("ServicePage", related_name="products")

    product = models.ForeignKey(
        Product,
        help_text=_("Select the product."),
        on_delete=models.PROTECT,
        related_name="services",
        verbose_name=_("product")
    )

    panels = [
        ProductChooserPanel("product"),
        FieldPanel("is_primary"),
    ]

    @property
    def description(self):
        return self.product.description

    @property
    def service(self):
        return self.page

    @property
    def title(self):
        return self.product.title

    @property
    def vendor(self):
        return self.product.vendor


class ServicePageRequest(Orderable):
    """Connect requests with services."""

    # Request is connected to the business (or other) service from the ServicePage.
    page = ParentalKey("ServicePage", related_name="requests")

    request = models.ForeignKey(
        Request,
        help_text=_("Select the request"),
        related_name="services",
        verbose_name=_("request")
    )

    panels = [
        RequestChooserPanel("request"),
    ]

    def __unicode__(self):
        return u"%s" % self.request.title

    @property
    def description(self):
        return self.request.description

    @property
    def grouping(self):
        return self.request.grouping

    @property
    def service(self):
        return self.page

    @property
    def show_in_all_groups(self):
        return self.request.show_in_all_groups

    @property
    def title(self):
        return self.request.title


class ServicePageTag(TaggedItemBase):
    """Associate tags with a service."""

    content_object = ParentalKey("ServicePage", related_name="tagged_items")


class SupportingService(Orderable):
    """Link a service to it's supporting services."""

    # TODO: Remove SupportingService.link_external since it can't really be used.

    # TODO: Rename link_page to supporting and page to dependent?

    # TODO: Remove properties and methods created to alias the supporting service.

    # OLA (document, link, or page)

    link_external = models.URLField(
        _("External Link"),
        blank=True,
        help_text=_("Enter the full URL if the service is not maintained in this tool."),
        max_length=1024,
        null=True,
    )

    link_page = models.ForeignKey(
        "ServicePage",
        null=True,
        blank=True,
        help_text=_("Link to another service maintained in this tool."),
        related_name='dependent_services',
        verbose_name=_("Link to Service")
    )

    page = ParentalKey("ServicePage", related_name="supporting_services")

    panels = [
        FieldPanel('link_external'),
        PageChooserPanel('link_page'),
    ]

    class Meta:
        ordering = ["sort_order"]
        verbose_name = _("Supporting Service")
        verbose_name_plural = _("Supporting Services")

    @property
    def assignment(self):
        return self.supporting_service.assignment

    @property
    def category(self):
        return self.supporting_service.category

    def get_link(self):
        """Get the actual URL from the local page or external link (in that order).

        :rtype: str

        """
        if self.link_page:
            return self.link_page.url
        else:
            return self.link_external

    def get_purpose_display(self):
        return self.supporting_service.get_purpose_display()

    def get_scope_display(self):
        return self.supporting_service.get_scope_display()

    def get_type_display(self):
        return self.supporting_service.get_type_display()

    @property
    def link(self):
        return self.get_link()

    @property
    def primary_service(self):
        return self.page

    @property
    def supporting_service(self):
        return self.link_page

    @property
    def title(self):
        return self.supporting_service.title


class ServicePage(Page):
    """A service."""

    # SLA (document, link, or page)

    assignment = models.ForeignKey(
        Group,
        blank=True,
        help_text=_("Select the group to which this service is assigned."),
        on_delete=models.SET_NULL,
        null=True,
        related_name="services",
        verbose_name=_("assignment")
    )

    budget = models.DecimalField(
        _("budget"),
        blank=True,
        decimal_places=2,
        help_text=_("Enter the budget for this service."),
        max_digits=10,
        null=True
    )

    category = models.ForeignKey(
        Category,
        help_text=_("Select the category."),
        on_delete=models.PROTECT,
        related_name="services",
        verbose_name=_("category")
    )

    description = models.TextField(
        _("description"),
        blank=True,
        help_text=_("A brief description of the service. May be shown to users. Optional, but recommended."),
        null=True
    )

    last_review_dt = models.DateTimeField(
        _("review date/time"),
        blank=True,
        help_text=_("Last time the service was reviewed."),
        null=True
    )

    lifecycle = models.ForeignKey(
        Lifecycle,
        blank=True,
        help_text=_("Select the lifecycle."),
        null=True,
        on_delete=models.SET_NULL,
        related_name="services",
        verbose_name=_("lifecycle")
    )

    next_review_dt = models.DateTimeField(
        _("next date/time"),
        blank=True,
        help_text=_("Next time the service should be reviewed."),
        null=True
    )

    CATALOG = "catalog"
    PIPELINE = "pipeline"
    RETIRED = "retired"
    PORTFOLIO_CHOICES = (
        (PIPELINE, _("Pipeline")),
        (CATALOG, _("Catalog")),
        (RETIRED, _("Retired")),
    )
    portfolio = models.CharField(
        _("portfolio"),
        choices=PORTFOLIO_CHOICES,
        default=CATALOG,
        help_text=_("Select the portfolio."),
        max_length=32
    )

    VERY_LOW = 1
    LOW = 2
    NORMAL = 3
    HIGH = 4
    VERY_HIGH = 5
    PRIORITY_CHOICES = (
        (VERY_LOW, _("Very Low")),
        (LOW, _("Low")),
        (NORMAL, _("Normal")),
        (HIGH, _("High")),
        (VERY_HIGH, _("Very High")),
    )
    priority = models.PositiveSmallIntegerField(
        _("priority"),
        default=NORMAL,
        choices=PRIORITY_CHOICES,
        help_text=_("The criticality or priority of the service to the business")
    )

    # TODO: "purpose" is actually described as "categories" in the ITIL documentation. What to do with the Category
    # model if we change it?

    CORE = "core"
    ENABLING = "enabling"
    ENHANCING = "enhancing"
    PURPOSE_CHOICES = (
        (CORE, _("Core")),
        (ENABLING, _("Enabling")),
        (ENHANCING, _("Enhancing")),
    )
    purpose = models.CharField(
        _("purpose"),
        choices=PURPOSE_CHOICES,
        default=CORE,
        help_text=_("Select the overall purpose of the service."),
        max_length=32
    )

    # TODO: scope could be automatic based on SupportingService records.

    PRIMARY = "primary"
    SUPPORTING = "supporting"
    SCOPE_CHOICES = (
        (PRIMARY, _("Primary")),
        (SUPPORTING, _("Supporting")),
    )
    scope = models.CharField(
        _("scope"),
        default=PRIMARY,
        choices=SCOPE_CHOICES,
        help_text=_("Select the scope of service."),
        max_length=32
    )

    tags = ClusterTaggableManager(through=ServicePageTag, blank=True)

    # TODO: Service Type in ITIL is internal and external.

    BUSINESS = "business"
    TECHNICAL = "technical"
    TYPE_CHOICES = (
        (BUSINESS, _("Business")),
        (TECHNICAL, _("Technical")),
    )
    type = models.CharField(
        _("type"),
        default=BUSINESS,
        choices=TYPE_CHOICES,
        help_text=_("Select the type of service."),
        max_length=32
    )

    content_panels = Page.content_panels + [
        FieldPanel("description", classname="full"),
        MultiFieldPanel(
            [
                FieldPanel("type"),
                FieldPanel("scope"),
                FieldPanel("purpose"),
                FieldPanel("priority"),
                CategoryChooserPanel("category"),
                FieldPanel("tags"),
            ],
            heading=_("Classification"),
        ),
        GroupChooserPanel("assignment"),
        InlinePanel("requests", label=_("Requests")),
        InlinePanel("products", label=_("Products")),
        InlinePanel("supporting_services", label=_("Supporting Services")),
        InlinePanel("agreements", label=_("Agreements")),
        MultiFieldPanel(
            [
                FieldPanel("budget"),
                FieldPanel("last_review_dt"),
                FieldPanel("next_review_dt"),
            ],
            heading=_("Review")
        ),
        MultiFieldPanel(
            [
                LifecycleChooserPanel("lifecycle"),
                FieldPanel("portfolio"),
            ],
            heading=_("Status")
        ),
    ]

    class Meta:
        verbose_name = _("Service")
        verbose_name_plural = _("Services")

    def get_context(self, request, *args, **kwargs):
        context = super(ServicePage, self).get_context(request, *args, **kwargs)

        context['active_page'] = "services"
        context['service'] = context['page']

        return context

    def get_dependents(self):
        """Get the services that depend on this service.

        :rtype: list
        :returns: Returns a list of ``ServicePage`` instances that depend this service.

        .. note::
            This is when ``SupportingService.link_page`` refers to this service instance.

        """
        a = list()

        qs = self.dependent_services.all()
        for join in qs:
            a.append(join.page)

        return a

    def get_menu_items(self):
        """Get the service requests associated with the service, including those that may be shared across all services.

        :rtype: list

        """
        a = list()

        qs = self.requests.all()
        for request in qs:
            a.append(request)

        qs = Request.objects.filter(show_in_all_groups=True)
        for request in qs:
            a.append(request)

        return a

    def get_supporting(self):
        """Get the services upon which this service depends.

        :rtype: list
        :returns: Returns a list of ``ServicePage`` instances that support this service.

        .. note::
            This is when ``SupportingService.page`` refers to this service instance.

        """
        a = list()

        qs = self.supporting_services.all()
        for join in qs:
            a.append(join.page)

        return a

    @property
    def has_agreements(self):
        return self.agreements.exists()

    @property
    def has_dependents(self):
        return self.dependent_services.exists()

    @property
    def has_products(self):
        return self.products.exists()

    @property
    def has_requests(self):
        return self.requests.exists()

    @property
    def has_supporting(self):
        return self.supporting_services.exists()

    @property
    def menu_items(self):
        return self.get_menu_items()
