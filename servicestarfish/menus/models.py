# Imports

from __future__ import unicode_literals
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailcore.models import Orderable
from wagtailmodelchooser.edit_handlers import register_chooser_for_model
from taxonomytiger.models import StringLookupModel

# Exports

__all__ = (
)

# Constants

AUTH_USER_MODEL = settings.AUTH_USER_MODEL

# Models


class Grouping(StringLookupModel):
    """A service request menu group."""

    class Meta:
        ordering = ["label"]
        verbose_name = _("Grouping")
        verbose_name_plural = _("Groupings")

GroupingChooserPanel = register_chooser_for_model(Grouping)


class Request(Orderable):
    """A service request menu item."""

    # comments

    description = models.TextField(
        _("description"),
        blank=True,
        help_text=_("A brief description of the item. Shown to users. Optional, but recommended"),
        null=True
    )

    grouping = models.ForeignKey(
        Grouping,
        help_text=_("Select the group in which this this menu item is displayed."),
        on_delete=models.PROTECT,
        related_name="items",
        verbose_name=_("Grouping")
    )

    show_in_all_groups = models.NullBooleanField(
        _("all groups"),
        default=False,
        help_text=_("Indicates this item is shown in all groups.")
    )

    title = models.CharField(
        _("title"),
        help_text=_("The name of the item as shown to users."),
        max_length=128
    )

    panels = [
        FieldPanel("title", classname="full title"),
        FieldPanel("description", classname="full"),
        GroupingChooserPanel("grouping"),
        FieldPanel("show_in_all_groups"),
    ]

    class Meta:
        ordering = ["sort_order"]
        verbose_name = _("Request")
        verbose_name_plural = _("Requests")

    def __unicode__(self):
        return u"%s" % self.title

    # def save(self, *args, **kwargs):
    #     if not self.grouping:
    #         self.show_in_all_groups = True
    #     else:
    #         self.show_in_all_groups = False
    #
    #     super(Request, self).save(*args, **kwargs)

RequestChooserPanel = register_chooser_for_model(Request)
