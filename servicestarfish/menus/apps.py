from __future__ import unicode_literals
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MenusConfig(AppConfig):
    name = 'servicestarfish.menus'
    label = "servicestarfish_menus"
    verbose_name = _("Menus")
