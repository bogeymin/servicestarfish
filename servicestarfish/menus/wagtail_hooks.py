# Imports

from django.utils.translation import ugettext_lazy as _
from wagtail.contrib.modeladmin.options import ModelAdmin
from models import Grouping, Request

# Models


class GroupingAdmin(ModelAdmin):
    model = Grouping
    menu_icon = "folder-inverse"

    list_display = [
        "label",
        "description",
    ]


class RequestAdmin(ModelAdmin):
    model = Request
    menu_icon = "folder-inverse"

    list_display = [
        "title",
        "description",
        "grouping",
        "show_in_all_groups",
    ]
    list_filter = [
        "grouping",
        "show_in_all_groups",
    ]
