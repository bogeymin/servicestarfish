# Imports

# from django.utils.translation import ugettext_lazy as _
from wagtail.contrib.modeladmin.options import ModelAdmin
from models import Product, Vendor

# Models


class ProductAdmin(ModelAdmin):
    model = Product
    menu_icon = "folder-inverse"

    list_display = [
        "title",
        "description",
        "vendor",
    ]
    list_filter = [
        "vendor",
    ]


class VendorAdmin(ModelAdmin):
    model = Vendor
    menu_icon = "folder-inverse"

    list_display = [
        "title",
    ]
