# Imports

from __future__ import unicode_literals
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtailmodelchooser.edit_handlers import register_chooser_for_model

# Exports

__all__ = (
)

# Constants

AUTH_USER_MODEL = settings.AUTH_USER_MODEL

# Models


class Vendor(models.Model):
    """A vendor for a product."""

    title = models.CharField(
        _("title"),
        help_text=_("The name of the vendor. May be shown to users."),
        max_length=128,
        unique=True
    )

    class Meta:
        ordering = ["title"]
        verbose_name = _("Vendor")
        verbose_name_plural = _("Vendors")

    def __unicode__(self):
        return u"%s" % self.title

VendorChooserPanel = register_chooser_for_model(Vendor)


class Product(models.Model):
    """A product (technology or service) used by a service."""

    # group: A Group could also be associated with or assigned to a product.

    description = models.TextField(
        _("description"),
        blank=True,
        help_text=_("A brief description of the product. May be shown to users. Optional, but recommended."),
        null=True
    )

    title = models.CharField(
        _("title"),
        help_text=_("The name of the product as shown to users."),
        max_length=128,
        unique=True
    )

    vendor = models.ForeignKey(
        Vendor,
        help_text=_("Select the vendor."),
        on_delete=models.PROTECT,
        related_name="products",
        verbose_name=_("vendor")
    )

    panels = [
        FieldPanel("title"),
        VendorChooserPanel("vendor"),
        FieldPanel("description"),
    ]

    class Meta:
        ordering = ["title"]
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    def __unicode__(self):
        return u"%s" % self.title

ProductChooserPanel = register_chooser_for_model(Product)
