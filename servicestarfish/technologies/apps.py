from __future__ import unicode_literals
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class TechnologiesConfig(AppConfig):
    name = 'servicestarfish.technologies'
    label = "servicestarfish_technologies"
    verbose_name = _("Technologies")
