# Imports

from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.models import Group
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db import models
from django.utils.translation import ugettext_lazy as _
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from taggit.models import TaggedItemBase
from wagtail.wagtailadmin.edit_handlers import InlinePanel, FieldPanel, MultiFieldPanel, PageChooserPanel, FieldRowPanel
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailsearch import index
from wagtailmodelchooser.edit_handlers import register_chooser_for_model
from lemurlink.models import RelatedLinkModel
from taxonomytiger.models import StringLookupModel
from servicestarfish.technologies.models import Product, ProductChooserPanel
from servicestarfish.menus.models import Request, RequestChooserPanel

GroupChooserPanel = register_chooser_for_model(Group, title_field="name")

# Exports

__all__ = (
)

# Constants

AUTH_USER_MODEL = settings.AUTH_USER_MODEL

# Models


class ProviderIndexPage(Page):
    """An index page for providers."""

    intro = RichTextField(
        _("intro"),
        blank=True,
        help_text=_("Brief introduction appearing before the list."),
        null=True
    )

    content_panels = Page.content_panels + [
        FieldPanel("intro", classname="full"),
    ]

    search_fields = Page.search_fields + [
        index.SearchField("intro"),
    ]

    subpage_types = [
        "ProviderPage",
    ]

    def get_subpages(self, criteria=None):
        """Get a list of packages."""
        qs = ProviderPage.objects.live().descendant_of(self)

        if isinstance(criteria, dict):
            qs = qs.filter(**criteria)

        return qs

    def get_context(self, request, *args, **kwargs):
        context = super(ProviderIndexPage, self).get_context(request, *args, **kwargs)

        objects = self.get_subpages()

        # Pagination
        page = request.GET.get('page')
        entries_per_page = 9
        paginator = Paginator(objects, entries_per_page)
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            objects = paginator.page(1)
        except EmptyPage:
            objects = paginator.page(paginator.num_pages)

        # Update template context
        context['active_page'] = "providers"
        context['subpages'] = objects

        return context

    @property
    def subpages(self):
        return self.get_subpages()


class ProviderPageGroup(Orderable):
    """Organize user groups into a provider."""

    description = RichTextField(
        _("description"),
        blank=True,
        help_text=_("Brief description of how this group contributes to the provider."),
        null=True
    )

    group = models.ForeignKey(
        Group,
        help_text=_("Select the group."),
        related_name="+",
        verbose_name=_("group")
    )

    is_primary = models.NullBooleanField(
        _("primary"),
        help_text=_("Indicates this is the primary group within the provider.")
    )

    provider = ParentalKey("ProviderPage", related_name="groups")

    panels = [
        GroupChooserPanel("group"),
        FieldPanel("is_primary"),
        FieldPanel("description"),
    ]


class ProviderPageLink(RelatedLinkModel, Orderable):
    """Connect links to a provider."""

    # There a few ways would could link a provider to a service.
    # 1. ServicePage.provider selects from the ProviderPage. This seems to be the most natural.
    # 2. ProviderPageLink.link_page is a ServicePage. This is more flexible, but much less obvious, and it circumvents
    # the natural benefits of Wagtail's page hierarchy.
    # 3. ProviderPage.subpage_types could include ServicePage (and possibly ServiceIndexPage). This uses Wagtail's page
    # hierarchy but forces all services to live under a ProviderPage, which is less flexible (and more work).
    #
    # We elected for the first approach, but have left ProviderPageLink because it may still be useful when detailing a
    # service provider.

    provider = ParentalKey("ProviderPage", related_name="links")


class ProviderPage(Page):
    """A service provider."""

    intro = RichTextField(
        _("intro"),
        blank=True,
        help_text=_("Brief introduction to the provider."),
        null=True
    )

    is_external = models.NullBooleanField(
        _("external"),
        help_text=_("Indicates the provider has customers outside of the organization.")
    )

    is_internal = models.NullBooleanField(
        _("internal"),
        help_text=_("Indicates the provider has customers within the organization.")
    )

    is_shared = models.NullBooleanField(
        _("shared"),
        help_text=_("Indicates the provider serves more than one internal business unit.")
    )

    content_panels = Page.content_panels + [
        FieldPanel("intro", classname="full"),
        InlinePanel("groups", label=_("Groups")),
        MultiFieldPanel(
            [
                FieldPanel("is_internal"),
                FieldPanel("is_shared"),
                FieldPanel("is_external"),
            ],
            heading=_("Scope of Operation")
        ),
        InlinePanel("links", _("Links")),
    ]

    search_fields = Page.search_fields + [
        index.SearchField("intro"),
    ]

    class Meta:
        verbose_name = _("Provider")
        verbose_name_plural = _("Providers")

    def get_services(self):
        """Get services offered by the provider.

        :rtype: QuerySet

        """
        from servicestarfish.services.models import ServicePage

        # FIXME: Right now it is quicker to return a list. Instead, we should return a qs that selects
        # ProviderPageLink where link_page is a ServicePage.
        a = list()

        qs = self.links.all()
        for row in qs:
            if issubclass(row.link_page, ServicePage):
                a.append(row.link_page)

        return a

    @property
    def has_services(self):
        """Indicates whether services have been associated with the provider."""
        # FIXME: Getting the services is really inefficient. See note on get_services().
        return len(self.get_services()) > 0
